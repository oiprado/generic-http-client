import { Component } from '@angular/core';
import { resolveResourceApi } from './@core/data/api-routes';
import * as jpath from 'jsonpath';
import { CompanyService } from './@core/data/services/company.service';
import { AuthService } from './@core/data/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [CompanyService, AuthService]
})
export class AppComponent {
  title = 'app';

  constructor(private companyService: CompanyService, private authService: AuthService){

  }

  api = resolveResourceApi();

  authenticate (){
     this.authService.token();
    
  }

  getCompaniesByUser(){
    this.companyService.getCompaniesByUser();
  }



}
