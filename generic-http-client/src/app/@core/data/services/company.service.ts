import { BaseService } from "../base-service";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { tap } from "rxjs/operators/tap";
import { log } from "../../util/log";

@Injectable()
export class CompanyService extends BaseService {

  private api: string = "api";

  constructor(private httpClient2: HttpClient){
    super(httpClient2);
  }

  create(){
    super.resolveApi(this.api, "create", {}).pipe(
      tap((data: any) =>{
        log(data);
      })
    );
  }

  update(){
    super.resolveApi(this.api, "update", {});
  }

  remove(){
    super.resolveApi(this.api, "delete", {});
  }

  getCompaniesByUser(){
    super.resolveApi(this.api, "getCompaniesByUser", {}).subscribe((data:any) => {
      log(data);
    });
  }

}
