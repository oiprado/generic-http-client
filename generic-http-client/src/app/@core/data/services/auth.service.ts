import { BaseService } from "../base-service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { pipe } from "rxjs";
import { tap } from "rxjs/operators";
import { log } from "../../util/log";

export class AuthResponse {
  access_token: string;
  token_type: string;
  refresh_token: string;
  expires_in: number;
  scope: string;
}

@Injectable()
export class AuthService extends BaseService {

  private api: string = "auth";

  constructor(private _httpClient: HttpClient) {
    super(_httpClient);
    // log(_httpClient);
  }

  token(){

    const params = new URLSearchParams();
    params.append('grant_type', "password");
    params.append('username', "user@mail.com");
    params.append('password', "admin1234");


    this.resolveApi(this.api, "token", params.toString()).subscribe((response: AuthResponse) => {
      localStorage.setItem("accessToken", response.access_token);
      localStorage.setItem("refreshToken", response.refresh_token);
    });
  }

};