import * as jpath from 'jsonpath';
import { resolveResourceApi } from './api-routes';
import { length, isNil } from 'ramda'
import { log } from '../util/log';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError, map, tap } from 'rxjs/operators';
// import { throwError as observableThrowError } from 'rxjs';

export class BaseService {

  private resourceApi: any = resolveResourceApi();

  private httpClient: HttpClient;

  constructor(httpClient: HttpClient) {
    this.httpClient = httpClient;
  }

  protected resolveApi(name: string, routeName: string, body, options?): Observable<any> {

    let apiResource: any = jpath.query(this.resourceApi, `$.${name}`)[0];
    let routeApi: any = jpath.query(this.resourceApi, `$.${name}.routes[?(@.name=="${routeName}")]`)[0];

    if (!isNil(apiResource)) {

      const { host, basePath, useToken, useBasicAuth, token } = apiResource;

      if (!isNil(routeApi)) {
        const { route, method, contentType, accept } = routeApi;
        let url = `${host}${basePath}${route}`;
        return this.request(url, body, method, contentType, accept, useToken, useBasicAuth, token, options).pipe(
          tap(response => response)
        );
      }
    }
  }

  private request = (url: string, body, method: string, contentType: string, accept:string, useToken: boolean, useBasicAuth: boolean, token: string, options?): Observable<any> => {

    log("----------")

    if (useToken.toString() === "true") {
      token = `Bearer ${localStorage.getItem('accessToken')}`;
    }
    log(url)
    log(`useBasicAuth: ${useBasicAuth}`);
    log(`useToken: ${useToken}`);
    log(`token: ${token}`);
    log(`content-type: ${contentType}`);
    log(`accept: ${accept}`)
    


    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Content-Type', contentType);
    if(!isNil(accept)){
      headers = headers.append('Accept', accept);
    }

    if (!isNil(token)) {
      headers = headers.append('Authorization', token)
    }

    log("----------")
    if (method === "post-params") {
      return this.postParams(url, body, headers);
    } else if (method === "post") {
      return this.post(url, body, headers);
    } else if (method === "put") {
      return this.put(url, body, headers);
    } else if (method === "get") {
      return this.get(url, body, headers);
    } else if (method === "getUrl") {
      return this.getUrl(url, body, headers);
    } else if (method == "delete") {
      return this.delete(url, body, headers);
    }
  }

  private postParams = (url: string, params, headers: HttpHeaders): Observable<Object> => {
    return this.httpClient.post(url, params, { headers: headers });
  }

  private post = (url: string, body, headers: HttpHeaders): Observable<Object> => {
    return this.httpClient.post(url, body, { headers: headers });
  }

  private get = (url: string, body, headers: HttpHeaders): Observable<Object> => {
    log('> get')
    return this.httpClient.post(url, body, { headers: headers });
  }

  private getUrl = (url: string, body, headers: HttpHeaders): Observable<Object> => {
    log('> getUrl')
    return this.httpClient.post(url, body, { headers: headers });
  }

  private put = (url: string, body, headers: HttpHeaders): Observable<Object> => {
    log('> put')
    return this.httpClient.post(url, body, { headers: headers });
  }

  private delete = (url: string, body, headers: HttpHeaders): Observable<Object> => {
    log('> delete')
    return this.httpClient.post(url, body, { headers: headers });
  }



  private handleError(error: HttpErrorResponse) {
    //console.error(error);

    // detect invalid access token



    return error;
  }


}