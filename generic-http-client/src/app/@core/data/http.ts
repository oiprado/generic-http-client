import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/finally';
import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { cond, equals, always, T, reduce, toPairs, replace, concat, forEachObjIndexed } from 'ramda';
import { resolveApiRoute } from './api-routes';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';


@Injectable()
export class HttpService {
  constructor(private router: Router, private httpClient: HttpClient) { }

  get(
    routeIdentifier: string,
    body?: Object,
    willSendToken?: boolean,
    options?: RequestOptions
  ): Observable<any> | any {
    return this.request('get', routeIdentifier, body, willSendToken, options);
  }

  getUrl(
    routeIdentifier: string,
    body?: Object,
    willSendToken?: boolean,
    options?: RequestOptions
  ): Observable<any> | any {
    return this.request('getUrl', routeIdentifier, body, willSendToken, options);
  }

  post(
    routeIdentifier: string,
    body?: Object,
    willSendToken?: boolean,
    options?: RequestOptions
  ): Observable<any> | any {
    const route: string = resolveApiRoute(routeIdentifier);
    return this.request('post', routeIdentifier, body, willSendToken, options);
  }

  put(
    routeIdentifier: string,
    body?: Object,
    willSendToken?: boolean,
    options?: RequestOptions
  ): Observable<any> | any {
    const route: string = resolveApiRoute(routeIdentifier);
    // console.log(body);
    this.httpClient.post(route, body);
    return this.request('put', routeIdentifier, body, willSendToken, options);
  }

  delete(
    routeIdentifier: string,
    body?: Object,
    willSendToken?: boolean,
    options?: RequestOptions
  ): Observable<any> | any {
    return this.request('delete', routeIdentifier, body, willSendToken, options);
  }

  private request(
    method: string,
    routeIdentifier: string,
    body?: Object,
    willSendToken?: boolean,
    options?: RequestOptions
  ): Observable<any> | any {
    const route: string = resolveApiRoute(routeIdentifier);
    const dataForUrl: string =
      reduce(
        (acc: string, value: any) => `${acc}${value[0]}=${value[1]}&`,
        '?',
        toPairs(body)
      ) || '';
    const token: string = localStorage.getItem('accessToken') || '';
    const headers: HttpHeaders = willSendToken
      ? new HttpHeaders({ Authorization: `bearer ${token}` })
      : new HttpHeaders({ 'Content-Type': 'application/json' });
    // const requestOptions: RequestOptions = options || new RequestOptions({ headers });

    if (method == 'get') {
      return this.intercept(
        this.httpClient
          .get(route + dataForUrl, {headers: headers})
          .map(this.extractData)
      );
    }

    if (method == 'getUrl') {
      return this.intercept(
        this.httpClient
          .get(this.urlResolve(route, body), {headers: headers})
          .map(this.extractData)
      );
    }

    if (method == 'delete') {
      // const requestOptions: RequestOptions = options || new RequestOptions({ headers: headers, body: body });

      // let HttpParams 
      return this.intercept(
        this.httpClient
          .delete(route, { headers: headers })
          .map(this.extractData)
          .catch(this.handleErrorObservable)
      )
    }

    if (method == 'post') {
      return this.intercept(
        this.httpClient
          .post(route, body, { headers: headers})
          .map(this.extractData)
          .catch(this.handleErrorObservable)
      )
    }

    if (method == 'put') {
      return this.intercept(
        this.httpClient
          .put(route, body, { headers: headers})
          .map(this.extractData)
          .catch(this.handleErrorObservable)
      )
    }
  }

  private handleErrorObservable(error: Response | any) {
    // console.error(error.message || error);
    return Observable.throw(error.message || error);
  }

  private urlResolve(url: string, body: Object): string {
    var urlResolve = (value, key) => {
      url = replace(concat(':', key), value, url);
    };
    forEachObjIndexed(urlResolve, body);
    return url;

  }

  private extractData(res: HttpResponse<any> | any): any {
    const message = res.json() || {};
    return message;
  }

  private intercept(observable: Observable<any>): Observable<any> {
    // console.log(observable);
    return observable.catch((error: Response | any): ErrorObservable => {
      const { status } = error;
      const body = error.json() || {};
      const message = cond([
        [equals(400), always(body)],
        [equals(401), () => this.unauthorised()],
        [equals(403), () => this.forbidden(body)],
        [T, always(`Error ${status}: ${body}`)],
      ])(status);
      return Observable.throw(message);
    });
  }

  private unauthorised() {
    this.router.navigate(['/forbidden']);
  }
  private forbidden(body) {
    window.alert(body);
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    this.router.navigate(['/forbidden']);
  }
}
