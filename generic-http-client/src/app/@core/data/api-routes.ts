// import { default as api } from '../../../assets/api.json';
import * as api from './config/api.json';
import * as imagesApi from './config/images-api.json';


const { host, basePath, routes } = (<any> api);

const { hostStorage, directory, local } = (<any> imagesApi);//require('./config/images-api.json');

export const resolveApiRoute = (identifier: string): string => {
  return `${host}${basePath}${routes[identifier]}`;
};

export const resolveImagesPath = (): string => {
  
  return `${local ? 'file://' : ''}${hostStorage}${directory}`;
};

export const resolveResourceApi = (): any => {
  return api;
};
